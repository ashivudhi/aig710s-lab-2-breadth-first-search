### A Pluto.jl notebook ###
# v0.14.3
#ASHIVUDHI ERRO REMAIDER

using Markdown
using InteractiveUtils

# ╔═╡ 5ff08e54-9dfe-11eb-2845-49f1fae2c9e3
md"# PROBLEM 1"

# ╔═╡ e1597c82-644f-4cb8-ba24-97d198deebf4
md"### Action Definition"

# ╔═╡ 3a217a9d-79d6-4f6b-a981-d828fc4e3af5
struct Action
	name::String
	cost::Int64
end

# ╔═╡ e9d057d7-7a52-4a54-9a47-9328aac4dec8
A1 = Action("Left",3)

# ╔═╡ 62da3e26-d2c3-408e-958b-e157e2845871
A2 = Action("Right",3)

# ╔═╡ d1eeed8d-0f53-4ad1-866c-ed8e59a1e5e3
A3 = Action("PickUp",5)

# ╔═╡ aeb172be-26dd-4ca4-a385-c8af10bfd96d
md"### State Definition"

# ╔═╡ ee292350-9e66-402f-9129-83e2f8b0d278
struct State
	name::String
	position::Int64
	dirt::Vector{Bool}
end	

# ╔═╡ 0138950d-af4f-4fa1-a530-0de090870c2f
St1 = State("State1",1,[true,true,true])

# ╔═╡ 47182d33-852f-4542-851f-2456bb7ea719
St2 = State("State2",2,[true,true,true])

# ╔═╡ 876ac032-c73e-4a8e-9877-a5c3a479fcbd
St3 = State("State3",3,[true,true,true])

# ╔═╡ 45efc538-6878-4baf-afa1-0998333c5d22
St4 = State("State4",1,[false,true,true])

# ╔═╡ 633ef955-6ad2-4523-b547-b10fad4cad27
St5 = State("State5",2,[false,true,true])

# ╔═╡ b79222ab-2489-46c7-9dca-b4e735bae3bd
St6 = State("State6",3,[false,true,true])

# ╔═╡ db7605f9-2135-4583-9943-3c0f05b3bf90
St7 = State("State7",3,[false,true,false])

# ╔═╡ 8ac6f05a-86f2-4397-87bb-7eb7020297d6
md"### Transition Model Definition"

# ╔═╡ 3efd8b3e-d40f-4789-854d-cc9cc6fdca25
TransModel = Dict()

# ╔═╡ a95ee7e2-7f05-45ce-9a23-c6ec4b7e5850
push!(TransModel, St1 => [(A2,St2),(A1,St1),(A3,St4)])

# ╔═╡ becba678-11b3-4b43-85fd-b5aedd206bc8
push!(TransModel, St2 => [(A2,St3),(A1,St1)])

# ╔═╡ 0f9159e2-37bd-4b30-8638-2d2a6cafd839
push!(TransModel, St3 => [(A2,St3),(A1,St2)])

# ╔═╡ f7f25ad7-cfc2-4475-a4b6-edc8be30a4d2
push!(TransModel, St4 => [(A2,St5),(A1,St4)])

# ╔═╡ 8a5937bd-00bd-4eb6-931f-76c8bf111f4a
push!(TransModel, St5 => [(A2,St6),(A1,St4)])

# ╔═╡ 37bd572c-41fb-438a-b1ef-8a98e25e52cb
push!(TransModel, St6 => [(A2,St6),(A1,St5),(A3,St7)])

# ╔═╡ 330c5bae-a0ba-4b3f-a0ba-4229478850b7
#push!(TransModel, St7 => [])
md"### B.F.Search Algorithms"

# ╔═╡ d7f0fd71-0585-4baa-858e-0990dd4f7a23
function TransSearch(TransModel,GoalState,InitState)
	St1 = true
	Result = []
	explored = []
	frontier = Queue{State}()
	
	enqueue!(frontier, InitState)
	
	while !isempty(frontier)
			current_state = dequeue!(frontier)
			push!(explored, current_state) #adding
			candidates = TransModel[current_state]
			if(St1)
				St1 = false
		else
			past_candidate = TransModel[explored[length(explored)]]
		for single_past_candidate in past_candidate
				if single_past_candidate[2] == current_state
					push!(result, single_past_candidate[1])
				end
			end
			end
		for single_candidate in candidates
			if!(single_candidate[2] in explored)
			if (single_candidate[2] in GoalState)
					return result
				else
					enqueue!(frontier, single_candidate[2])
				end
			end	
	end
end
	

# ╔═╡ Cell order:
# ╠═5ff08e54-9dfe-11eb-2845-49f1fae2c9e3
# ╠═e1597c82-644f-4cb8-ba24-97d198deebf4
# ╠═3a217a9d-79d6-4f6b-a981-d828fc4e3af5
# ╠═e9d057d7-7a52-4a54-9a47-9328aac4dec8
# ╠═62da3e26-d2c3-408e-958b-e157e2845871
# ╠═d1eeed8d-0f53-4ad1-866c-ed8e59a1e5e3
# ╠═aeb172be-26dd-4ca4-a385-c8af10bfd96d
# ╠═ee292350-9e66-402f-9129-83e2f8b0d278
# ╠═0138950d-af4f-4fa1-a530-0de090870c2f
# ╠═47182d33-852f-4542-851f-2456bb7ea719
# ╠═876ac032-c73e-4a8e-9877-a5c3a479fcbd
# ╠═45efc538-6878-4baf-afa1-0998333c5d22
# ╠═633ef955-6ad2-4523-b547-b10fad4cad27
# ╠═b79222ab-2489-46c7-9dca-b4e735bae3bd
# ╠═db7605f9-2135-4583-9943-3c0f05b3bf90
# ╠═8ac6f05a-86f2-4397-87bb-7eb7020297d6
# ╠═3efd8b3e-d40f-4789-854d-cc9cc6fdca25
# ╠═a95ee7e2-7f05-45ce-9a23-c6ec4b7e5850
# ╠═becba678-11b3-4b43-85fd-b5aedd206bc8
# ╠═0f9159e2-37bd-4b30-8638-2d2a6cafd839
# ╠═f7f25ad7-cfc2-4475-a4b6-edc8be30a4d2
# ╠═8a5937bd-00bd-4eb6-931f-76c8bf111f4a
# ╠═37bd572c-41fb-438a-b1ef-8a98e25e52cb
# ╠═330c5bae-a0ba-4b3f-a0ba-4229478850b7
# ╠═d7f0fd71-0585-4baa-858e-0990dd4f7a23
